package be.bstorm.formation.aeroport;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
@EnableConfigurationProperties
public class Bf2023MobileSpringAeroportApplication {

    public static void main(String[] args) {
        SpringApplication.run(Bf2023MobileSpringAeroportApplication.class, args);
    }

}
