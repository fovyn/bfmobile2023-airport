package be.bstorm.formation.aeroport.presentation_layer.controllers;

import be.bstorm.formation.aeroport.business_layer.services.UserService;
import be.bstorm.formation.aeroport.dataaccess_layer.entities.security.UserEntity;
import be.bstorm.formation.aeroport.presentation_layer.models.AuthResponse;
import be.bstorm.formation.aeroport.presentation_layer.models.LoginForm;
import be.bstorm.formation.aeroport.presentation_layer.utils.JwtUtil;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

@RestController
public class SecurityController {
    private final JwtUtil utils;
    private final PasswordEncoder passwordEncoder;
    private final UserService securityService;

    public SecurityController(JwtUtil utils, PasswordEncoder passwordEncoder, UserService securityService) {
        this.utils = utils;
        this.passwordEncoder = passwordEncoder;
        this.securityService = securityService;
    }

    @PostMapping(path = {"/signIn"})
    public ResponseEntity<AuthResponse> signInAction(
            HttpServletRequest request,
            @RequestBody LoginForm form
    ) {
        System.out.println(request);
        UserDetails user = this.securityService.loadUserByUsername(form.username);

        if (passwordEncoder.matches(form.password, user.getPassword())) {
            return ResponseEntity.ok(new AuthResponse(utils.generateToken(user), user));
        }

        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
    }

    @GetMapping(path = {"/register"})
    public ResponseEntity<AuthResponse> getregisterAction(
            @RequestBody LoginForm form
    ) {
//        return ResponseEntity.ok(null);
        UserEntity entity = new UserEntity();
        entity.setUsername(form.username);
        entity.setPassword(passwordEncoder.encode(form.password));

        UserDetails user = this.securityService.insert(entity);
        return ResponseEntity.ok(new AuthResponse(utils.generateToken(user), user));
    }

    @PostMapping(path = {"/register"})
    public ResponseEntity<AuthResponse> registerAction(
            @RequestBody LoginForm form
    ) {
//        return ResponseEntity.ok(null);
        UserEntity entity = new UserEntity();
        entity.setUsername(form.username);
        entity.setPassword(passwordEncoder.encode(form.password));

        UserDetails user = this.securityService.insert(entity);
        return ResponseEntity.ok(new AuthResponse(utils.generateToken(user), user));
    }
}
