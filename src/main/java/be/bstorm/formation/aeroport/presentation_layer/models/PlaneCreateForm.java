package be.bstorm.formation.aeroport.presentation_layer.models;

import be.bstorm.formation.aeroport.dataaccess_layer.entities.PlaneEntity;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

import java.time.LocalDate;

@Data
public class PlaneCreateForm {
    @NotBlank
    private String imma;
    @Min(value = 0L)
    private long ownerId;

    public PlaneEntity toEntity() {
        PlaneEntity entity = new PlaneEntity();
        entity.setImma(imma);
        entity.setBuyDate(LocalDate.now());

        return entity;
    }
}
