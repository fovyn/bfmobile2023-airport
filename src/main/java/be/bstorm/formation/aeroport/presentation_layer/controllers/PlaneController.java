package be.bstorm.formation.aeroport.presentation_layer.controllers;

import be.bstorm.formation.aeroport.business_layer.services.PlaneService;
import be.bstorm.formation.aeroport.dataaccess_layer.entities.PersonEntity;
import be.bstorm.formation.aeroport.dataaccess_layer.entities.PlaneEntity;
import be.bstorm.formation.aeroport.presentation_layer.models.Plane;
import be.bstorm.formation.aeroport.presentation_layer.models.PlaneCreateForm;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = {"/planes"})
public class PlaneController {
    private final PlaneService planeService;

    @Autowired
    public PlaneController(PlaneService planeService) {
        this.planeService = planeService;
    }

    @PreAuthorize(value = "hasAnyAuthority('ROLE_USER') and hasAuthority('GROUP_RH')")
    @GetMapping(path = {"", "/"})
    public ResponseEntity<List<Plane>> getAllAction(
            @RequestParam(defaultValue = "0", required = false)
            Integer page,
            @RequestParam(defaultValue = "5", required = false)
            Integer size
    ) {
        Page<PlaneEntity> response = this.planeService.findAll(page, size);

        List<Plane> planes = response.stream()
                .map(Plane::fromBLL)
                .toList();

        return ResponseEntity.ok(planes);
    }

    @GetMapping(path = {"/{id}"})
    public ResponseEntity<Plane> getOneAction(
            @PathVariable(name = "id") int id
    ) {
        PlaneEntity plane = this.planeService.findOneById(id)
                .orElseThrow();

        return ResponseEntity.ok(Plane.fromBLL(plane));
    }
    @GetMapping(path= {"/{id}/owner"})
    public ResponseEntity<PersonEntity> getPlaneOwnerAction(
            @PathVariable(name = "id") int id
    ) {
        PlaneEntity plane = this.planeService.findOneById(id)
                .orElseThrow();

        return ResponseEntity.ok(plane.getOwner());
    }

    @PostMapping(path = {"", "/"})
    public ResponseEntity<Plane> postPlaneAction(
            @Valid @RequestBody PlaneCreateForm form
    ) {
        PlaneEntity entity = form.toEntity();

        this.planeService.insert(entity, form.getOwnerId());

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(Plane.fromBLL(entity));
    }

}
