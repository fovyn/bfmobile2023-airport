package be.bstorm.formation.aeroport.presentation_layer.models;

import be.bstorm.formation.aeroport.dataaccess_layer.entities.PlaneEntity;
import lombok.Builder;
import lombok.Getter;

import java.time.LocalDate;

@Builder
@Getter
public class Plane {
    private Long id;
    private String imma;
    private LocalDate buyDate;

    public static Plane fromBLL(PlaneEntity bll) {
        Plane.PlaneBuilder builder = new Plane.PlaneBuilder()
                .id(bll.getId())
                .imma(bll.getImma())
                .buyDate(bll.getBuyDate());

        return builder.build();
    }
}
