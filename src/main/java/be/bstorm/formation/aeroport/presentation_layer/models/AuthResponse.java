package be.bstorm.formation.aeroport.presentation_layer.models;

import be.bstorm.formation.aeroport.dataaccess_layer.entities.security.UserEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;


@RequiredArgsConstructor
public class AuthResponse {
    public final String token;
    public final UserDetails user;
}
