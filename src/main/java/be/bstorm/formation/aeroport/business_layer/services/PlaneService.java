package be.bstorm.formation.aeroport.business_layer.services;

import be.bstorm.formation.aeroport.dataaccess_layer.entities.PlaneEntity;
import org.springframework.data.domain.Page;

import java.util.Optional;

public interface PlaneService {
    Page<PlaneEntity> findAll(int page, int offset);
    Optional<PlaneEntity> findOneById(int id);

    void insert(PlaneEntity entity, long ownerId);
}
