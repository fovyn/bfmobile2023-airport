package be.bstorm.formation.aeroport.business_layer.services;

import be.bstorm.formation.aeroport.dataaccess_layer.entities.PersonEntity;
import be.bstorm.formation.aeroport.dataaccess_layer.entities.PlaneEntity;
import be.bstorm.formation.aeroport.dataaccess_layer.repositories.PersonRepository;
import be.bstorm.formation.aeroport.dataaccess_layer.repositories.PlaneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PlaneServiceImpl implements PlaneService {
    private final PlaneRepository planeRepository;
    private final PersonRepository personRepository;

    @Autowired
    public PlaneServiceImpl(PlaneRepository planeRepository, PersonRepository personRepository) {
        this.planeRepository = planeRepository;
        this.personRepository = personRepository;
    }

    @Override
    public Page<PlaneEntity> findAll(int page, int size) {
        return this.planeRepository.findAll(PageRequest.of(page, size));
    }

    @Override
    public Optional<PlaneEntity> findOneById(int id) {
        return this.planeRepository.findById(id);
    }

    @Override
    public void insert(PlaneEntity entity, long ownerId) {
        PersonEntity owner = this.personRepository.findById(ownerId).orElseThrow();

        entity.setOwner(owner);

        this.planeRepository.save(entity);
    }
}
