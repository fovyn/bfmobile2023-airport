package be.bstorm.formation.aeroport.business_layer.models;

import be.bstorm.formation.aeroport.dataaccess_layer.entities.PersonEntity;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;

@Data
@Builder
public class PersonBll {
    private Long id;
    public String fullName;

    public static PersonBll fromEntity(PersonEntity entity) {
        return new PersonBllBuilder()
                .id(entity.getId())
                .fullName(entity.getLastname()+ " "+ entity.getFirstname())
                .build();
    }
    public PersonEntity toEntity() {
        String[] fullName = this.fullName.split(" ");
        PersonEntity person = new PersonEntity();

        person.setId(this.id);
        person.setFirstname(fullName[1]);
        person.setLastname(fullName[0]);

        return person;
    }
}
