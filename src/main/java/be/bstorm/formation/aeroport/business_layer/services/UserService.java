package be.bstorm.formation.aeroport.business_layer.services;

import be.bstorm.formation.aeroport.dataaccess_layer.entities.security.UserEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {
    UserDetails insert(UserEntity entity);
}
