package be.bstorm.formation.aeroport.dataaccess_layer.repositories;

import be.bstorm.formation.aeroport.dataaccess_layer.entities.PlaneEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PlaneRepository extends JpaRepository<PlaneEntity, Integer> {

    @Query(value = "SELECT p FROM Plane p WHERE p.imma = :imma")
    Optional<PlaneEntity> findOneByImma(@Param("imma") String imma);
}
