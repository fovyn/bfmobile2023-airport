package be.bstorm.formation.aeroport.dataaccess_layer.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.Data;

import java.util.List;

@Entity(name = "Pilot")
@Table(name = "pilot")
@Data
public class PilotEntity extends PersonEntity {
    private String brevet;

    @OneToMany(targetEntity = FlightEntity.class, mappedBy = "pilot")
    private List<FlightEntity> flights;
}
