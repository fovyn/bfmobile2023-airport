package be.bstorm.formation.aeroport.dataaccess_layer.entities;

import jakarta.persistence.*;
import lombok.Data;

@Entity(name = "PlaneType")
@Data
@Table(name = "plane_type")
public class PlaneTypeEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String constructor;
    private String type;
    private int power;
    private int nbPlace;
}
