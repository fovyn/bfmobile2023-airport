package be.bstorm.formation.aeroport.dataaccess_layer.entities;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
public class FlightEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private PilotEntity pilot;
    @ManyToOne
    private PlaneTypeEntity planeType;

    private int nbFlight = 1;
}
