package be.bstorm.formation.aeroport.dataaccess_layer.entities;

import jakarta.persistence.*;
import lombok.Data;

import java.time.LocalDate;

@Entity(name = "Plane")
@Table(name = "plane", schema = "airport")
@Data
public class PlaneEntity extends AuditingBaseEntity {
    @Column(nullable = false)
    private String imma;
    @Column(name= "buy_date", nullable = false)
    private LocalDate buyDate;

    @ManyToOne(targetEntity = PersonEntity.class)
    @JoinColumn(name = "owner_id", nullable = false)
    private PersonEntity owner;
}
