package be.bstorm.formation.aeroport.dataaccess_layer.repositories;

import be.bstorm.formation.aeroport.dataaccess_layer.entities.EngineerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EngineerRepository extends JpaRepository<EngineerEntity, Long> {
}
