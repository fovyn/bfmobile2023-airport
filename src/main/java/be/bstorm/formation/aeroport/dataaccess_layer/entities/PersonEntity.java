package be.bstorm.formation.aeroport.dataaccess_layer.entities;

import jakarta.persistence.*;
import lombok.Data;

@Entity(name= "Person")
@Table(name= "person", schema = "airport")
@Data
@Inheritance(strategy = InheritanceType.JOINED)
public class PersonEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String firstname;
    @Column(nullable = false)
    private String lastname;

    @Embedded
    private Address address;
}
