package be.bstorm.formation.aeroport.dataaccess_layer.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

import java.time.LocalDate;
import java.time.LocalTime;

@Entity(name = "Maintenance")
@Table(name = "maintenance")
public class MaintenanceEntity extends AuditingBaseEntity {

    private String description;
    private LocalDate workDate;
    private LocalTime duration;

    @ManyToOne(targetEntity = PlaneEntity.class)
    private PlaneEntity plane;
    @ManyToOne(targetEntity = EngineerEntity.class)
    private EngineerEntity worker;
    @ManyToOne(targetEntity = EngineerEntity.class)
    private EngineerEntity checker;
}
