package be.bstorm.formation.aeroport.dataaccess_layer.repositories;

import be.bstorm.formation.aeroport.dataaccess_layer.entities.PersonEntity;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PersonRepository extends JpaRepository<PersonEntity, Long>, JpaSpecificationExecutor<PersonEntity> {
    List<PersonEntity> findAll(Specification<PersonEntity> specification);
}
