package be.bstorm.formation.aeroport.dataaccess_layer.entities;

import jakarta.persistence.*;
import lombok.Data;

import java.util.List;

@Entity(name = "engineer")
@Data
public class EngineerEntity extends PersonEntity {

    @ManyToMany(targetEntity = PlaneTypeEntity.class)
    private List<PlaneTypeEntity> habilitation;
}
